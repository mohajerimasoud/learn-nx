// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Footer, Header } from '@learn-mono2/ui-shared';
import { environment } from '../environments/environment';

export function App() {
  return (
    <>
       <Header></Header>
      <div>environment : {environment.production}</div>
      <Footer></Footer>
      <div />
    </>
  );
}

export default App;
