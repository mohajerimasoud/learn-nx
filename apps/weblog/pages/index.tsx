import { Footer, Header } from '@learn-mono2/ui-shared';

export function Index() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.scss file.
   */
  return (
   <>
   <Header></Header>
   <h3>Title</h3>
   <Footer></Footer>
   </>
  );
}

export default Index;
